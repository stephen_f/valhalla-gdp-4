﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Tile : MonoBehaviour
{

    public Material tileme;
    public Vector2 EndValue;
    public Vector2 StartValueY;
    public float AnimSpeed = 0.5f;

	// Use this for initialization
	void Start ()
	{
	    tileme.mainTextureScale = StartValueY;
        tileme.DOTiling(EndValue, AnimSpeed).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutElastic);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
