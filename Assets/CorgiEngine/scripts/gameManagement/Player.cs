﻿using System;
using UnityEngine;

public class Player
{
    public State PlayerState;
    public string Name;
    public PlayerNumber Number;
    public bool CanMove;
    public bool CanSpawn;

    public int Score;
    static public int PlayersCount;

    public GameObject PlayerGameObject;

    public enum State
    {
        Active,
        Inactive,
        Dead
    }

    public Player()
    {

        
        Name = "Noname" + PlayersCount;
        Number = (PlayerNumber) PlayersCount;
        CanSpawn = true;
        CanMove = true;
        Score = 0;

        PlayersCount++;
    }


    public enum PlayerNumber
    {
        Player1,
        Player2,
        Player3,
        Player4
    }
}