﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Weapon parameters
/// </summary>
public class Weapon : MonoBehaviour 
{
	/// the projectile type the weapon shoots
	public Projectile Projectile;
	/// the firing frequency
	public float FireRate;
	/// center of rotation of the gun
	public GameObject GunRotationCenter;
	/// the particle system to instantiate every time the weapon shoots
	public ParticleSystem GunFlames;
	/// the shells the weapon emits
	public ParticleSystem GunShells;	
	/// the initial projectile firing position
	public Transform ProjectileFireLocation;
	// the sound to play when the player shoots
	public AudioClip GunShootFx;

    private float lastShoot;

    void Start()
	{
		SetGunFlamesEmission (false);
		SetGunShellsEmission (false);
	}
	
	public void SetGunFlamesEmission(bool state)
	{
		GunFlames.enableEmission=state;	
	}
	
	public void SetGunShellsEmission(bool state)
	{
		GunShells.enableEmission=state;	
	}

    public void GunRotate(Vector3 rotation)
    {
        GunRotationCenter.transform.rotation = Quaternion.Euler(rotation);
    }

    public virtual void InstProjectile(Vector2 direction)
    {
        
        if (Time.time > lastShoot)
        {
            SetGunFlamesEmission(true);
            SetGunShellsEmission(true);

            // we instantiate the projectile at the projectileFireLocation's position.				
            var projectile = (Projectile)Instantiate(Projectile, ProjectileFireLocation.position, ProjectileFireLocation.rotation);
            if (GetComponentInParent<CorgiController>() != null)
            {
                projectile.Initialize(gameObject.transform.parent.gameObject, direction, GetComponentInParent<CorgiController>().Speed);
            }
            else
            {
                projectile.Initialize(gameObject.transform.parent.gameObject, direction);
            }
            
            
            // we play the shooting sound
            if (GunShootFx != null)
                SoundManager.Instance.PlaySound(GunShootFx, transform.position);


            lastShoot = Time.time + FireRate;
        }				

       
    }

    public void Flip()
    {
        if (GunShells != null)
            GunShells.transform.eulerAngles = new Vector3(GunShells.transform.eulerAngles.x, GunShells.transform.eulerAngles.y + 180, GunShells.transform.eulerAngles.z);
        if (GunFlames != null)
            GunFlames.transform.eulerAngles = new Vector3(GunFlames.transform.eulerAngles.x, GunFlames.transform.eulerAngles.y + 180, GunFlames.transform.eulerAngles.z);
    }
}
