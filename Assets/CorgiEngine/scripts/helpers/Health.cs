﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


/// <summary>
/// Adds this class to an object so it can have health (and lose it)
/// </summary>
public class Health : MonoBehaviour, CanTakeDamage
{
    /// the current amount of health of the object
    public int CurrentHealth;
    /// the points the player gets when the object's health reaches zero
    public int PointsWhenDestroyed;
    /// the effect to instantiate when the object takes damage
    public GameObject HurtEffect;
    /// the effect to instantiate when the object gets destroyed
    public GameObject DestroyEffect;

    public GameObject LootDrop;

    public AudioClip ReciveHit;


    protected int _startHealth;

     public void Awake()
    {
        _startHealth = CurrentHealth;
    }

    

    /// <summary>
    /// What happens when the object takes damage
    /// </summary>
    /// <param name="damage">Damage.</param>
    /// <param name="instigator">Instigator.</param>
    virtual public void TakeDamage(int damage, GameObject instigator)
    {
        // when the object takes damage, we instantiate its hurt effect
        Instantiate(HurtEffect, instigator.transform.position, transform.rotation);

        if (ReciveHit != null)
        {
            SoundManager.Instance.PlaySound(ReciveHit, transform.position);
        }

        //pause game
        //StartCoroutine(PauseGame(0.02f));

        // and remove the specified amount of health	
        CurrentHealth -= damage;
        // if the object doesn't have health anymore, we destroy it
        if (CurrentHealth <= 0)
        {
            DestroyObject();
            return;
        }


        Camera.main.DOShakePosition(0.1f, Vector3.up, 1);

        // We make the character's sprite flicker

            Color initialColor = GetComponentInChildren<Renderer>().material.color;
            Color flickerColor = new Color32(255, 20, 20, 255);
            Flicker(initialColor, flickerColor, 0.2f);
       
    }

    private IEnumerator PauseGame(float f)
    {


        // АХТУНГ РЕЙКАСТЫ ГЛЮЧИТ ?
        //Time.timeScale = 0f;


        Time.timeScale = .0000001f;
        yield return  new WaitForSeconds(f * Time.timeScale);
        Time.timeScale = 1.0f;

        //Time.timeScale = 1f;


    }


    /// <summary>
    /// Coroutine used to make the character's sprite flicker (when hurt for example).
    /// </summary>
    void Flicker(Color initialColor, Color flickerColor, float flickerSpeed)
    {
        var renderers = GetComponentsInChildren<Renderer>();

        for (int i = 0; i < 5; i++)
        {
            foreach (var mat in renderers)
            {
                if (!mat.material.HasProperty("_Color"))
                    continue;
                // Create new Sequence object
                Sequence mySequence = DOTween.Sequence();
                // Add tween 
                mySequence.Append(mat.material.DOColor(flickerColor, flickerSpeed / 2));
                // Add  , using Append so it will start after the previously added tweens end
                mySequence.Append(mat.material.DOColor(initialColor, flickerSpeed / 2));


            }
        }
        
        
      

        // makes the character colliding again with layer 12 (Projectiles) and 13 (Enemies)
        Physics2D.IgnoreLayerCollision(9, 12, false);
        Physics2D.IgnoreLayerCollision(9, 13, false);
    }


    /// <summary>
    /// Destroys the object
    /// </summary>
    protected virtual void DestroyObject()
    {
        SendMessage("OnDestroyHealth",SendMessageOptions.DontRequireReceiver);

        // instantiates the destroy effect
        if (DestroyEffect != null)
        {
            var instantiatedEffect = (GameObject)Instantiate(DestroyEffect, transform.position, transform.rotation);
            instantiatedEffect.transform.localScale = transform.localScale;
            if (LootDrop != null)
            {
                var loot = (GameObject)Instantiate(LootDrop, transform.position, transform.rotation);
            }
        }
        // Adds points if needed.
        if (PointsWhenDestroyed != 0)
        {
            //GameManager.Instance.AddPoints(PointsWhenDestroyed);
        }

        // destroys the object
        gameObject.SetActive(false);


        CheckSmartCube();
    }

    protected virtual void CheckSmartCube()
    {

        if (GetComponent<SmartCube>() != null)
        {
            if (GetComponent<SmartCube>().Mounted)
            {
                SendMessageUpwards("CubeDestroySafe", GetComponent<SmartCube>().index);
            }
        }
    }
}
