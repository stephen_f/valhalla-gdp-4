The Corgi Engine is a complete Unity 2D platformer engine.
Basically it'll allow you to create your own platformer game. 
It comes complete with lots of assets, animations, particle effects, etc...

I HAVE A LOT OF QUESTIONS !
---------------------------

Have you read the FAQ at http://www.reuno.net/corgi-engine-the-best-unity-2d-platformer-kit#faq ?
You should read it.

WHAT's IN THE CORGI ENGINE ?
----------------------------

The Corgi Engine contains quite a lot of stuff. 
Everything is in the Assets folder, and in it you'll find the following folders :

- animations : contains all the animations in the game

- fonts : the fonts used in the GUI

- materials : all the materials and associated sprites used in the particle effects

- resources : all the game's prefabs. You just have to drag one of these in your scene and they'll be ready

- scenes : a start screen and some demo levels (from the start screen press A on an xbox controller, or space on your keyboard)

- scripts : all the game scripts. This includes camera scripts, character controllers, enemy AI, health management, ladders, jumpers, level management, etc... It's all sorted for you in the right folders.

- sprites : all the sprites and spritesheets used in the game. Feel free to reuse them in your own game.


WHAT AM I SUPPOSED TO DO WITH IT ?
----------------------------------

The Corgi Engine includes everything you need to start your own platformer game.
You can either start fresh and pick the scripts you need from it, or modify the existing demo levels brick by brick, adding your own sprites and scripts as you go.


MOBILE INPUT
------------

You can activate / desactivate mobile inputs via the menu in your editor's top bar (the bar with File, Edit, etc...).
There's a "Mobile Input" entry there, just enable or disable it. Disabling it will hide all mobile inputs.
It should also automatically enable or disable it depending on your target platform when building.
Note that these mobile controls are just here as placeholders, it's just the Unity Standard Asset's inputs. You should replace them with your own.

DOCUMENTATION
-------------

A complete documentation is available for this asset, go to http://reuno.info/unity/corgi-engine/docs/


