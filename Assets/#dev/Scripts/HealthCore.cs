﻿using UnityEngine;
using System.Collections;

public class HealthCore : Health
{
    public bool CanDead = false;
    public bool OneShootKill = false;
    public Vector3 Respawn;


    public void Start()
    {
        Respawn = FindObjectOfType<Respawn>().GetPostionAt(1);
    }

    public override void TakeDamage(int damage, GameObject instigator)
    {
        if (OneShootKill && CanDead)
        {
            CurrentHealth = 1;
        }

        base.TakeDamage(damage, instigator);
    }

    protected override void DestroyObject()
    {
        if (CanDead)
        {
            transform.root.transform.position = Respawn;
            CurrentHealth = _startHealth;
        }
        else
        {
            // instantiates the destroy effect
            if (DestroyEffect != null)
            {
                var instantiatedEffect = (GameObject)Instantiate(DestroyEffect, transform.position, transform.rotation);
                instantiatedEffect.transform.localScale = transform.localScale;
            }



            CheckSmartCube();
        }
    }

    protected override void CheckSmartCube()
    {
        if (CanDead)
        {

            //GameManager.Instance.KillPlayer(transform.root.gameObject.GetComponent<PlayerInput>().PlayerNumber);

            transform.root.transform.position = Vector3.forward;

        }
        else
        {
            if (GetComponent<SmartCube>() != null)
            {
                if (GetComponent<SmartCube>().Mounted)
                {
                    //SendMessageUpwards("CubeDestroySafe", GetComponent<SmartCube>().index);
                }
            }

            // Сброс ХП
            CurrentHealth = _startHealth;
        }

    }
}
