﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Здесь происходит только detection, сам дэш запускаеться в CharacterBehavior
/// </summary>
public class DashKnockbackDetection : MonoBehaviour
{
    public bool CanCollide = true;
    private CharacterBehavior _characterBehavior;

    public void Start()
    {
        _characterBehavior = GetComponent<CharacterBehavior>();
    }
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!_characterBehavior.BehaviorState.Dashing)
            return;

            //работает на кубы
            if (collision.gameObject.GetComponent<Rigidbody2D>() != null)
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 100,ForceMode2D.Impulse);
            }


        if (collision.gameObject.tag == "DashKnockback")
        {
                if (CanCollide)
                {
                    StartCoroutine(ApplyDashKnockback(collision.gameObject));
                    CanCollide = false;
                }
        }

    }

    private IEnumerator ApplyDashKnockback(GameObject gameObjectCol)
    {

        if (gameObjectCol.transform.root.GetComponent<CorgiController>() != null)
        {
            gameObjectCol.transform.root.GetComponent<Feedback>().Flicker(Color.red, 0.2f);

            yield return new WaitForSeconds(0.1f);
            var forceDir = GetComponent<CorgiController>().Speed.normalized;
            gameObjectCol.transform.root.GetComponent<CharacterBehavior>().DashKnockback(forceDir);
        }


        yield return new WaitForSeconds(1f);
        CanCollide = true;
    }
}
