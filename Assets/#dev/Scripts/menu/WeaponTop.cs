﻿using UnityEngine;
using System.Collections;

public class WeaponTop : Weapon {
    public override void InstProjectile(Vector2 direction)
    {
        direction = Vector2.up;

        base.InstProjectile(direction);

    }
}
