﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CrateBoxRespawner : MonoBehaviour {

    public GameObject CrateBoxPrefab;
    public GameObject ItemPrefab;

    void Start ()
    {
       var clone = Instantiate(CrateBoxPrefab, transform.position, transform.rotation) as GameObject;
       clone.transform.SetParent(transform);

    }
	
    
	
	void OnWreck()
	{
        SpawnLoot();
    }

    public void SpawnLoot()
    {
        
        var loot = Instantiate(ItemPrefab, transform.position, transform.rotation) as GameObject;
        var startScale = loot.transform.localScale;
        loot.GetComponent<CubeProxy>().CrateBoxRespawner = this;

        loot.transform.DOScale(Vector3.one*0.1f, 0.01f);
        loot.transform.DOScale(startScale, 1f);
        loot.transform.DOShakePosition(0.4f,1f,1);

    }
}
