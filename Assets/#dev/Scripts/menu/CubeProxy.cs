﻿using UnityEngine;
using System.Collections;

public class CubeProxy : MonoBehaviour
{

    public CrateBoxRespawner CrateBoxRespawner;
    public GameObject ButtonHintX;
    public GameObject ButtonHintR1;

    public void Start()
    {

    }

    public void Update()
    {
        //Текст flip'ается вместе с родителем
        //костыль
        if (ButtonHintR1 !=null)
            ButtonHintR1.transform.position = transform.position;
    }

    public void OnBlockControllerDestroy()
    {
        Destroy(ButtonHintR1);
        CrateBoxRespawner.SpawnLoot();
    }

    public void OnMounted()
    {
        ButtonHintX.SetActive(false);
        ButtonHintR1.SetActive(true);
        ButtonHintR1.transform.SetParent(null);

    }

}
