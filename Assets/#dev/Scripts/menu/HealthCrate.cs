﻿using UnityEngine;
using System.Collections;

public class HealthCrate : Health
{
    public GameObject Wrecked;
    public bool canWreck = false;

    protected override void DestroyObject()
    {
       
        if (canWreck)
            Wreck();


        base.DestroyObject();


    }

    private void Wreck()
    {
        if (Wrecked != null)
            Instantiate(Wrecked, transform.position, transform.rotation);

        SendMessageUpwards("OnWreck", SendMessageOptions.DontRequireReceiver);
    }
}
