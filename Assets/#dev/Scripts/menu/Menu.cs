﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    public int ActivePlayers;
    public int ReadyToPlayerPlayers;


    private bool p1CanPress = true;
    private bool p2CanPress = true;
    private bool p3CanPress = true;
    private bool p4CanPress = true;

    

    public void Update()
    {
        if (ReadyToPlayerPlayers == ActivePlayers)
        {
            if (ReadyToPlayerPlayers >= 2)
            {
                StartGame();
            }
        }



        if (Input.GetButtonDown("P1_A") && p1CanPress)
        {
            p1CanPress = false;

            SpawnPlayer(Player.PlayerNumber.Player1);
        }
        if (Input.GetButtonDown("P2_A")&& p2CanPress)
        {
            p2CanPress = false;
            SpawnPlayer(Player.PlayerNumber.Player2);
        }
        if (Input.GetButtonDown("P3_A")&& p3CanPress)
        {
            p3CanPress = false;
            SpawnPlayer(Player.PlayerNumber.Player3);
        }
        if (Input.GetButtonDown("P4_A")&& p4CanPress)
        {
            p1CanPress = false;
            SpawnPlayer(Player.PlayerNumber.Player4);
        }

        
    }

    private  void SpawnPlayer(Player.PlayerNumber PlayerNumber)
    {
        ActivePlayers++;
        GameManager.Instance.SpawnPlayer(PlayerNumber);
        LevelManager.Instance.MenuPositions(PlayerNumber);
    }

    void StartGame()
    {
        GameManager.Instance.PlayersCountSetting = ActivePlayers;
        GameManager.Instance.RemovePlayers();
        LevelManager.Instance.SkipRound();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            ReadyToPlayerPlayers++;
            GameManager.Instance.PlayersToSpawn.Add(collision.gameObject.transform.root.gameObject.GetComponent<PlayerInput>().PlayerNumber);

            collision.gameObject.SetActive(false);
        }

    }

}
