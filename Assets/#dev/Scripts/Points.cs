﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Points : MonoBehaviour
{

    public int startPoints = 10;

    public int p1;
    public int p2;
    public int p3;
    public int p4;

    public Text hudP1;
    public Text hudP2;
    public Text hudP3;
    public Text hudP4;

    // Use this for initialization
    void Start()
    {
        p1 = p2 = p3 = p4 = startPoints;
        hudUpdate();
    }

    
    void Update()
    {
        

    }

    void subsPoints(string playerName)
    {
        if (playerName == "Player1")
            p1--;
        if (playerName == "Player2")
            p2--;
        if (playerName == "Player3")
            p3--;
        if (playerName == "Player4")
            p4--;
        hudUpdate();
        
    }
    void hudUpdate ()
    {
        hudP1.text = p1.ToString();
        hudP2.text = p2.ToString();
        hudP3.text = p3.ToString();
        hudP4.text = p4.ToString();
    }

}
