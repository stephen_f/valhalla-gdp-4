﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class q_PickUp : MonoBehaviour
{

    public int QuestStepNumb;

    public AudioClip PickUp_sfx;

    public void Start()
    {
        transform.DOPunchPosition(Vector3.up, 5f,1).SetLoops(-1, LoopType.Yoyo); 
        transform.DOPunchRotation(Vector3.forward * 45, 5f).SetLoops(-1, LoopType.Yoyo); 
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
            return;


       

        Colllect();

    }



    public void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetButtonDown("P1_Y"))
        {
            
        }
    }

    private void Colllect()
    {
        if(PickUp_sfx != null)
        {
            SoundManager.Instance.PlaySound(PickUp_sfx, transform.position);
        }

        FindObjectOfType<QuestSystem>().QuestSteps[QuestStepNumb] = true;
        SoundManager.Instance.PlaySound(FindObjectOfType<QuestSystem>().QuestStepCompleteSfx, transform.position);
        gameObject.SetActive(false);
    }
}
