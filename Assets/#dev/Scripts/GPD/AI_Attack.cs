﻿using UnityEngine;
using System.Collections;
using PhotoshopFile;

public class AI_Attack : MonoBehaviour
{

    public Transform SwordTarget;
    public float AttackRate = 1f;
    /// a melee collider2d (box, circle...), preferably attached to the character.
    public GameObject MeleeCollider;
    /// the duration of the attack, in seconds
    public float MeleeAttackDuration = 0.3f;


    private float _lastAttack =0f;

    // Use this for initialization
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {


    }

    public void OnAttack()
    {
        if (Time.time > _lastAttack + AttackRate)
        {
            
            Melee();

            _lastAttack = Time.time;
        }


    }

    private void Melee()
    {
        // we turn the melee collider on			
        MeleeCollider.SetActive(true);
        // we start the coroutine that will end the melee state in 0.3 seconds (tweak that depending on your animation)
        StartCoroutine(MeleeEnd());
    }

    /// <summary>
    /// Coroutine used to stop the melee attack after a delay
    /// </summary>
    private IEnumerator MeleeEnd()
    {
        BroadcastMessage("AttackTo", SwordTarget.position);
        // after 0.3 seconds, we end the melee state
        yield return new WaitForSeconds(MeleeAttackDuration);
        // reset state
        MeleeCollider.SetActive(false);
        
    }
}
