﻿using UnityEngine;
using System.Collections;

public class Stats : MonoBehaviour {

	public int Attack = 0;
	public int Defence = 0;


	
	void Start ()
	{
	   InvokeRepeating("CheckChildren",1f,1f);


	}

    private void CheckChildren()
    {
        Attack = GetComponentsInChildren<AttackBoost>().Length + 1;
        Defence = GetComponentsInChildren<DefenceBoost>().Length;

    }
}
