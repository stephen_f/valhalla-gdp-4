﻿using UnityEngine;
using System.Collections;
using Prime31;

public class SetCam : MonoBehaviour
{

    public CameraKit2D camscript ;
    public Collider2D TargetCollider;

	// Use this for initialization
	void Awake ()
	{

	    camscript = FindObjectOfType<CameraKit2D>();
        camscript.targetCollider = TargetCollider;

    }
	
	// Update is called once per frame
	void Update ()
	{
	    
	}
}
