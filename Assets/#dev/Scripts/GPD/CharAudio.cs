﻿using UnityEngine;
using System.Collections;

public class CharAudio : MonoBehaviour
{

    public AudioClip Landing;
    public AudioClip runStart;
    private CorgiController _controller; 
	// Use this for initialization
	void Start ()
	{

	    _controller = gameObject.GetComponent<CorgiController>();

	}
	
	// Update is called once per frame
	void Update () {

	    if (_controller.State.JustGotGrounded)
	    {
	        PlaySFX(Landing);
	    }

	    if (Input.GetButtonDown(GetComponent<PlayerInput>().Buttons.playerButtonRun) && (_controller.Speed.magnitude >.5))
	    {
	        PlaySFX(runStart);
	    }

	}

    private void PlaySFX(AudioClip sfx)
    {
        SoundManager.Instance.PlaySound(sfx, transform.position);
    }
}
