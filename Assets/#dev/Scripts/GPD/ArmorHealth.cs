﻿using UnityEngine;
using System.Collections;

public class ArmorHealth : Health
{
    public int ArmorLevel;
    public AudioClip ProtectSound;

    public override void TakeDamage(int damage, GameObject instigator)
    {
        
        


        if (damage - ArmorLevel <= 0)
        {
            SoundManager.Instance.PlaySound(ProtectSound, transform.position);
        }
        else
        {
            base.TakeDamage(damage - ArmorLevel, instigator);
        }
    }
}
