﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour
{

    public Transform Target;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Invoke("SetTarget", 1f);

        if (Target == null)
            return;

        transform.position = Target.position;

    }

    void SetTarget()
    {


    }
}
