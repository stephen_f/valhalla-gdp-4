﻿using UnityEngine;
using System.Collections;

public class DamageZoneFollow : MonoBehaviour
{

    public Transform Follower;

	// Use this for initialization
	void Start () {
	Invoke("SetFollower", 1f);
	}
	
	// Update is called once per frame
	void Update ()
    {
	if(Follower == null)
            return;

	    Follower.position = transform.position;

    }

    void SetFollower()
    {
        Follower = transform.root.gameObject.GetComponent<CharacterMelee>().MeleeCollider.transform;
        if (Follower == null)
        {
            Follower = transform.root.gameObject.GetComponent<AI_Attack>().MeleeCollider.transform;
        }
    }
}
