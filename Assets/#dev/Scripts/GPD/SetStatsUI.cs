﻿using System;
using UnityEngine;
using System.Collections;
using Prime31;
using UnityEngine.UI;

public class SetStatsUI : MonoBehaviour
{

    public GameObject Player;
    public Slider HealthSlider;
    public Text Strenght;
    private HealthCore _playerHelathCore;
    private Stats _playerStats;


    // Use this for initialization
    private void Start()
    {

        Invoke("SetPlayer", 1f);

    }

    // Update is called once per frame
    private void Update()
    {
        if(_playerHelathCore == null)
            return;


        Strenght.text = _playerStats.Attack.ToString();
        

        HealthSlider.value = _playerHelathCore.CurrentHealth;
    }

    private void SetPlayer()
    {

        Player = Camera.main.GetComponent<CameraKit2D>().targetCollider.gameObject;

        _playerStats = Player.GetComponent<Stats>();
        _playerHelathCore = Player.GetComponentInChildren<HealthCore>();

        HealthSlider.maxValue = _playerHelathCore.CurrentHealth;
    }

}
