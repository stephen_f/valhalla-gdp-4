﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PickUpSign : MonoBehaviour
{

    public GameObject AnimatedSprite;

	// Use this for initialization
	void Start ()
	{

        AnimatedSprite.transform.DOPunchPosition(Vector3.up, 5f, 1).SetLoops(-1, LoopType.Restart);
        AnimatedSprite.transform.DOPunchRotation(Vector3.forward * 45, 5f).SetLoops(-1, LoopType.Restart);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
