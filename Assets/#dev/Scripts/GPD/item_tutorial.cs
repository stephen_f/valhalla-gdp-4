﻿using UnityEngine;
using System.Collections;

public class item_tutorial : MonoBehaviour
{

    public GameObject respawn_prefab;
    public Transform StartPoint1;
    public Transform StartPoint2;

    private GameObject Spawned; 

	// Use this for initialization
	void Start ()
    {

        Spawned = Instantiate(respawn_prefab, StartPoint1.position, StartPoint1.rotation) as GameObject;
	 
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Spawned == null)
	    {
            Spawned = Instantiate(respawn_prefab, StartPoint2.position, StartPoint2.rotation) as GameObject;
        }
	
	}
}
