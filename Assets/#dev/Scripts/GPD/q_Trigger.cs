﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class q_Trigger : MonoBehaviour
{

    public int QuestStepNumb;
    public AudioClip trigger_sfx;
    public void Start()
    {
       
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
            return;


       

        Colllect();

    }



    public void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetButtonDown("P1_Y"))
        {
            
        }
    }

    private void Colllect()
    {
        if (trigger_sfx != null)
        {
            SoundManager.Instance.PlaySound(trigger_sfx, transform.position);
        }

        FindObjectOfType<QuestSystem>().QuestSteps[QuestStepNumb] = true;
        SoundManager.Instance.PlaySound(FindObjectOfType<QuestSystem>().QuestStepCompleteSfx, transform.position);
        gameObject.SetActive(false);
    }
}
