﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{

    public Transform Player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        Invoke("SetPlayer", 1f);

	    if (Player == null)
            return;


        transform.position = Player.position;
	    
           

	}

    void SetPlayer()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;

    }
}
