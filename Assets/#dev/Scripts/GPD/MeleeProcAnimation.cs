﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MeleeProcAnimation : MonoBehaviour
{
    public Transform SwordIK;
    public float FirstDuration = 0.1f;
    public float SeconDuration = 0.5f;
    public Ease SwordEase;
    public Ease ArmEase;
    public Vector3 StartLocalPosition;

    public float FirstRotation = 0f;
    public float EndRotation = 0f;

    public float FirstRotationDuration = 0.1f;
    public float EndRotationDuration = 0.5f;

    public AudioClip SwordSound;

    public bool RandomRotations;

    public bool UseRandomRange;
    public float RandomValueFrom;
    public float RandomValueTo;

    private Sequence s;
    public float StayDuration = 0.2f;

    void Start ()
    {
        StartLocalPosition = SwordIK.localPosition;
    }
	
	
	void Update () {
        
	}

    void AttackTo(Vector3 target)
    {
        StartCoroutine("AttackIE", target);
    }

    IEnumerator AttackIE(Vector3 target)
    {
        if (RandomRotations)
        {
            if(UseRandomRange)
            {
                FirstRotation = Random.Range(RandomValueFrom, RandomValueTo);
                EndRotation = Random.Range(RandomValueFrom, RandomValueTo);

            }
            else
            {
                FirstRotation = Random.Range(0, 360);
                EndRotation = Random.Range(360, 450);
            }
            
        }


        yield return new WaitForSeconds(Random.Range(0.1f,0.5f));
        s.Complete();


        s = DOTween.Sequence();
        
        var halfTarget = new Vector3(target.x , target.y + 1f, target.z);
        var halfTarget2 = new Vector3(target.x, target.y - 1f, target.z);



        s.Append(SwordIK.DOMove(halfTarget, FirstDuration).SetEase(SwordEase));
        s.Append(SwordIK.DOMove(target, FirstDuration).SetEase(SwordEase)).OnStart(Sound);
        s.Append(SwordIK.DOMove(target, StayDuration).SetEase(SwordEase));
        s.Append(SwordIK.DOMove(halfTarget2, FirstDuration).SetEase(SwordEase));
        s.Append(SwordIK.DOLocalMove(StartLocalPosition, SeconDuration).SetEase(ArmEase));
        

        s.Insert(0, SwordIK.DOLocalRotate(Vector3.forward* FirstRotation, FirstRotationDuration, RotateMode.FastBeyond360));
        s.Append(SwordIK.DOLocalRotate(Vector3.forward*EndRotation, EndRotationDuration, RotateMode.FastBeyond360));
    }


    public void Sound()
    {
        SoundManager.Instance.PlaySound(SwordSound, transform.position);
    }

    public void Flipped()
    {
        s.Complete();
        s.Rewind();
    }


}
