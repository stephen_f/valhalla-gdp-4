﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestSystem : MonoBehaviour
{

    public List<bool> QuestSteps = new List<bool>();
    public AudioClip QuestStepCompleteSfx;
    public AudioClip SmsSfx;

    public GameObject q2_eat_step2_talk_shaverm;
    public GameObject q2_eat_step4_talk_shaverm;

    public GameObject sms_talk_end;

    public GameObject EndGameScreen;

    private bool QuestComplete;
    private bool GameComplete;



    void Start ()
	{
        
        //[0] = q1_drink;
        //[1] = q2_eat_step3_kill_miniboss;
        //[2] = q2_eat_step4_talk2;

        //[3] = q2_eat_step1_talk_magazin_optional;
        //[4] = q2_eat_step2_talk_shaverm_optional;

        //[5] = End Game
        //[6] = Phone
        QuestSteps.Add(false);
        QuestSteps.Add(false);
        QuestSteps.Add(false);
        QuestSteps.Add(false);
        QuestSteps.Add(false);
        QuestSteps.Add(false);
        QuestSteps.Add(false);
    }
	
	
	void Update ()
    {
        
	    if (QuestSteps[0] && QuestSteps[1] && QuestSteps[2] )
	    {
            if(QuestComplete)
                return;



            //Debug.Log("GAME COMPLETE");
            SoundManager.Instance.PlaySound(SmsSfx, sms_talk_end.transform.position);
            sms_talk_end.SetActive(true);
            Invoke("EndGame",10f);

	        QuestComplete = true;

	    }

	    if (QuestSteps[1])
	    {
            q2_eat_step4_talk_shaverm.SetActive(true);
            q2_eat_step2_talk_shaverm.SetActive(false);
        }

	    if (QuestSteps[5])
	    {
            

	    }
    }

    private void EndGame()
    {
        //End Game
        if (GameComplete)
            return;
        SoundManager.Instance.PlaySound(QuestStepCompleteSfx, sms_talk_end.transform.position);
        EndGameScreen.SetActive(true);

        GameComplete = true;
    }
}
