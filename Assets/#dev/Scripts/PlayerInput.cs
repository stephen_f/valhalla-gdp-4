﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{
    public PlayersButtons Buttons;
    private Player.PlayerNumber _playerNumber;

    private  CharacterBehavior _player;
    private  CorgiController _controller;

    public Player.PlayerNumber PlayerNumber
    {
        get { return _playerNumber; }
        set
        {
            _playerNumber = value;

            
            //Каждый раз когда меняем управляем пеерсоздаем buttons
            Buttons = new PlayersButtons(_playerNumber);
        }
    }

    public void Awake()
    {
        Buttons = new PlayersButtons(_playerNumber);

        _player = GetComponent<CharacterBehavior>();
        _controller = GetComponent<CorgiController>();
    }

    public void Update()
    {
        

        
        PlayerMove();

        PlayerRun();


        PlayerJump();

        PlayerDash();
			
				
		PlayerMelee();		
		
		PlayerShoot();

		PlayerJetpack();


        PlayerPickup();

    }

    private void PlayerPickup()
    {
        if (Input.GetButtonDown(Buttons.playerButtonPickUp))
        {
            BroadcastMessage("PickUp",SendMessageOptions.DontRequireReceiver);
        }
    }

    private void PlayerJetpack()
    {
        if (_player.GetComponent<CharacterJetpack>() != null)
        {
            if ((Input.GetButtonDown(Buttons.playerButtonDash) || Input.GetButton(Buttons.playerButtonDash)))
                _player.GetComponent<CharacterJetpack>().JetpackStart();

            if (Input.GetButtonUp(Buttons.playerButtonDash))
                _player.GetComponent<CharacterJetpack>().JetpackStop();
        }
    }

    private void PlayerMelee()
    {
        if (_player.GetComponent<CharacterMelee>() != null)
        {
            if (Input.GetButtonDown(Buttons.playerButtonAttack))
                _player.GetComponent<CharacterMelee>().Melee();
        }
    }

    private void PlayerShoot()
    {
       

            //if (Input.GetButtonDown(Buttons.playerButtonAttack))
            //    BroadcastMessage("ShootOnce");

            //if (Input.GetButton(Buttons.playerButtonAttack))
            //    BroadcastMessage("ShootStart");

            //if (Input.GetButtonUp(Buttons.playerButtonAttack))
            //   BroadcastMessage("ShootStop");

            
       
    }

    private void PlayerDash()
    {
        if (Input.GetButtonDown(Buttons.playerButtonDash))
            _player.Dash();
    }

    private void PlayerMove()
    {
        _player.SetHorizontalMove(Input.GetAxis(Buttons.playerAxisH));
        _player.SetVerticalMove(Input.GetAxis(Buttons.playerAxisV));
    }

    private void PlayerRun()
    {
        if ((Input.GetButtonDown(Buttons.playerButtonRun) || Input.GetButton(Buttons.playerButtonRun)))
            _player.RunStart();

        if (Input.GetButtonUp(Buttons.playerButtonRun))
            _player.RunStop();
    }

    private void PlayerJump()
    {
        if (Input.GetButtonDown(Buttons.playerButtonJump))
        {
            if (!_player.BehaviorState.InDialogueZone)
            {
                _player.JumpStart();
               
            }
        }

        if (Input.GetButtonUp(Buttons.playerButtonJump))
        {
            _player.JumpStop();
           
        }
    }
}