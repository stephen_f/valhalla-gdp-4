﻿using UnityEngine;

public class DebugGameTesting : MonoBehaviour
{
    private CubeSpawner _spawner;


	// Use this for initialization
	void Start ()
	{
	    _spawner = FindObjectOfType<CubeSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Alpha1))
	    {

            GameManager.Instance.CreatePlayers();
	    }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameManager.Instance.KillPlayer(Player.PlayerNumber.Player1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
           GameManager.Instance.SpawnPlayer(Player.PlayerNumber.Player1);

        }
         if (Input.GetKeyDown(KeyCode.C))
	    {
            _spawner.Spawn();
	    }
        if (Input.GetKey(KeyCode.V))
        {
            Application.LoadLevel(Application.loadedLevel);
        }

       

        if (Input.GetKeyDown(KeyCode.N))
        {
          
        }
    }

}
