﻿using UnityEngine;
using System.Collections;

public class PickUpSkill : p2d_Skill
{
    public GameObject PickUpCollider;
    public GameObject PickupEffect;

    public void Awake()
    {
        PickUpCollider.SetActive(false);
    }

    public void PickUp()
    {
        if (Time.time > lastCast)
        {

            lastCast = Time.time + castDelay;

            StartCoroutine(UsePickup());

        }

    }

    private IEnumerator UsePickup()
    {
        PickUpCollider.SetActive(true);
        var effect = Instantiate(PickupEffect, transform.position, transform.rotation) as GameObject;
        effect.transform.SetParent(gameObject.transform);

        yield return new WaitForSeconds(0.5f);
        PickUpCollider.SetActive(false);

    }
}
