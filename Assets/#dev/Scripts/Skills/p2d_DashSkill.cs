﻿using UnityEngine;
using System.Collections;

public class p2d_DashSkill : p2d_Skill
{
    public float boostDuration = .5f;
    public float boostLenght = 10f;

    private bool canBoost = true;
    private float boostCooldown = 2f;


    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        Debug.Log(Buttons);
    }

    private void useDashSkill()
    {
        if (Time.time > lastCast)
        {
            lastCast = Time.time + castDelay;

            StartCoroutine(Boost(boostDuration));
            //Start the Coroutine called "Boost", and feed it the time we want it to boost us
        }
    }

    private IEnumerator Boost(float boostDur)
        //Coroutine with a single input of a float called boostDur, which we can feed a number when calling
    {
        p2d_PlayerMovement playerMovement = GetComponent<p2d_PlayerMovement>();
        playerMovement.CanMove = false;
        float playerDir = playerMovement.PlayerDirection;
        Vector2 boostSpeed = new Vector2(boostLenght*playerDir, 0);


        float time = 0; //create float to store the time this coroutine is operating
        canBoost = false; //set canBoost to false so that we can't keep boosting while boosting


        while (boostDur > time)
            //we call this loop every frame while our custom boostDuration is a higher value than the "time" variable in this coroutine
        {
            time += Time.deltaTime;
            //Increase our "time" variable by the amount of time that it has been since the last update
            GetComponent<Rigidbody2D>().velocity = boostSpeed;
            //set our rigidbody velocity to a custom velocity every frame, so that we get a steady boost direction like in Megaman
            yield return 0; //go to next frame
        }
        playerMovement.CanMove = true;

        yield return new WaitForSeconds(boostCooldown); //Cooldown time for being able to boost again, if you'd like.
        canBoost = true; //set back to true so that we can boost again.
    }
}