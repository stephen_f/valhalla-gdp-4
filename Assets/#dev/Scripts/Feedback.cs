﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Feedback : MonoBehaviour {


    

    public void Flicker(Color flickerColor, float flickerSpeed)
    {


        Color initialColor = GetComponentInChildren<Renderer>().material.color;
        

        var renderers = GetComponentsInChildren<Renderer>();

       
            foreach (var mat in renderers)
            {
                
                if (!mat.material.HasProperty("_Color"))
                    continue;
                // Create new Sequence object
                Sequence mySequence = DOTween.Sequence();
                // Add tween 
                mySequence.Append(mat.material.DOColor(flickerColor, flickerSpeed / 2));
                // Add  , using Append so it will start after the previously added tweens end
                mySequence.Append(mat.material.DOColor(initialColor, flickerSpeed/2));


            }
       


    }


    public void Shake()
    {
    }

    public void Shake(float duration, float strenght, int vibratio)
    {
        Camera.main.DOShakePosition(duration, strenght, vibratio);
    }

    public void ShakeGroundTouch()
    {
        Camera.main.DOShakePosition(0.2f, 1f, 1);
    }


    public void ShakeGroundTouch(int sizeMod)
    {
        Camera.main.DOShakePosition(0.2f, sizeMod, 1);
    }
}
