﻿using UnityEngine;
using System.Collections;

public class CubeSpawner : MonoBehaviour
{

    public GameObject SpawnPointsParent;
    private Transform[] SpawnPoints;
    public GameObject[] Cubes;


    // Use this for initialization
	void Start ()
	{
        SpawnPoints = SpawnPointsParent.GetComponentsInChildren<Transform>();
	    Spawn();


	}

    public void Spawn()
    {
        foreach (var point in SpawnPoints)
        {
            Instantiate(Cubes[Random.Range(0, Cubes.Length)], point.position, point.rotation);
            
        }
    }

    

    
}
