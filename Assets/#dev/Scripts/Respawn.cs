﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Сейчас получает рандомное респ, надо сделать по умнее
/// </summary>
public class Respawn : MonoBehaviour 
{
    private ArrayList respawns = new ArrayList();

    private int orderPoint = 0;

    void UpdateRespawns()
    {
        foreach (Transform child in gameObject.transform)
        {
            respawns.Add(child);
        }

    }

    public Vector3 GetRandomPosition()
    {
        var randomPoint = Random.Range(0, respawns.Count);

        Transform tmp = (Transform)respawns[randomPoint];
        
        return tmp.position;
    }

    public Vector3 GetOrderPosition()
    {
        Transform tmp = (Transform)respawns[orderPoint];
        orderPoint++;

        if (respawns[orderPoint] == null)
            orderPoint = 0;

        return tmp.position;
    }

    public Vector3 GetPostionAt(int count)
    {
        Transform tmp = (Transform)respawns[count];

        return tmp.position;

    }

    void Start()
    {
        UpdateRespawns();
    }
}
