﻿    using UnityEngine;
using System.Collections;
using DG.Tweening;


public class SmartCube : MonoBehaviour
{
    public Transform SlotArms1;
    public Transform SlotArms2;
    public Transform SlotLeg1;
    public Transform SlotLeg2;
    public Transform SlotHead;
    public float Mass = 10f;
    public int index;

    [ShowOnly]
    public bool HeadMode = false;
    [ShowOnly]
    public bool LegMode = false;
   
    public bool CanPickup = false;
    
    public bool Mounted = false;

    public float FireOnLevelDelay = 1f;
    public AudioClip PickUpSfx;


    private void Start()
    {
        
        if (GetComponent<Rigidbody2D>() != null)
        {
            GetComponent<Rigidbody2D>().mass = Mass;
        }
        CheckMode();
    }

    private void CheckMode()
    {
        if (!Mounted)
        {
           // StartCoroutine(Fire());
        }
        else
        {
            SendMessage("OnMounted", SendMessageOptions.DontRequireReceiver);
        }

    }

    private IEnumerator Fire()
    {
        var dirRnd = Random.Range(0, 1);
        Vector2 direction;
        if (dirRnd == 0)
            direction = Vector2.left;
        else
        {
            direction = Vector2.right;
        }



        BroadcastMessage("InstProjectile", direction,SendMessageOptions.DontRequireReceiver);

        yield return new WaitForSeconds(0.2f);
        BroadcastMessage("SetGunFlamesEmission", false, SendMessageOptions.DontRequireReceiver);
        BroadcastMessage("SetGunShellsEmission", false, SendMessageOptions.DontRequireReceiver);


        yield return new WaitForSeconds(FireOnLevelDelay+Random.Range(FireOnLevelDelay/15f, FireOnLevelDelay / 5f));
        
        
        

        CheckMode();
    }

    // Update is called once per frame
    private void Update()
    {
       
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerPickUp")
        {
            if (CanPickup)
            {
                if (!collision.gameObject.transform.parent.GetComponentInChildren<CubeController>().Full)
                {
                    PickUp();
                    StartCoroutine("CallAdd", collision);
                }
            }
        }
    }

    public void SetModeHead(bool on)
    {
        HeadMode = on;

       
    }

    public void SetModeLeg(bool on)
    {
        LegMode = on;

    }

    private void PickUp()
    {
       
            Mounted = true;
            CanPickup = false;

            if (gameObject.GetComponent<Rigidbody2D>() != null)
            {
                //Убирает только на след. кадр, по этому я сделал корутину
                Destroy(gameObject.GetComponent("Rigidbody2D"));
                SoundManager.Instance.PlaySound(PickUpSfx, transform.position);
            }
            if (gameObject.GetComponent<PickUpSign>() != null)
            {
                
                Destroy(gameObject.GetComponent<PickUpSign>().AnimatedSprite);
                 Destroy(gameObject.GetComponent("PickUpSign"));
        }
            


    }


    private IEnumerator CallAdd(Collider2D collision)
    {
        yield return new WaitForSeconds(0.2f);

        collision.gameObject.transform.parent.BroadcastMessage("CubeAdd", this);

        //Visual Feedback
        transform.DOMove(collision.transform.position, 0.1f);
        transform.DOScale(Vector3.zero, 0.2f);
        Destroy(gameObject, 2f);
    }

    private void SelfDestruct()
    {
        transform.DOScale(Vector3.zero, 0.5f);
        Destroy(gameObject,1f);
    }

    
    public void OnCollisionEnter2D(Collision2D collision)
    {
        
    }


   

}