﻿public  class PlayersButtons
{
    //Static variables are shared across all instances
    //of a class. 
    public static int playerCount;

    public string playerAxisH;
    public string playerAxisH2;
    public string playerAxisV;
    public string playerAxisV2;
    public string playerButtonJump;
    public string playerButtonRun;
    public string playerButton2;
    public string playerButtonPickUp;
    public string playerButtonDash;
    public string playerButtonAttack;


    /// <summary>
    /// Клавиши управления
    /// </summary>
    /// <param name="playerNumber"></param>
    public PlayersButtons(Player.PlayerNumber playerNumber)
    {
        playerCount++;

        if (playerNumber == Player.PlayerNumber.Player1)
        {
            playerAxisH = "P1_Horizontal";
            playerAxisV = "P1_Vertical";
            playerButtonJump = "P1_A";
            playerButtonPickUp = "P1_Y";
            playerButtonDash = "P1_B";
            playerButtonAttack = "P1_X";

            playerButtonRun = "P1_B"; 
}
        if (playerNumber == Player.PlayerNumber.Player2)
        {
            playerAxisH = "P2_Horizontal";
            playerAxisV = "P2_Vertical";
            playerButtonJump = "P2_A";
            playerButtonPickUp = "P2_X";
            playerButtonDash = "P2_LB";
            playerButtonAttack = "P2_RB";
        }
        if (playerNumber == Player.PlayerNumber.Player3)
        {
            playerAxisH = "P3_Horizontal";
            playerAxisV = "P3_Vertical";
            playerButtonJump = "P3_A";
            playerButtonPickUp = "P3_X";
            playerButtonDash = "P3_LB";
            playerButtonAttack = "P3_RB";
        }
        if (playerNumber == Player.PlayerNumber.Player4)
        {
            playerAxisH = "P4_Horizontal";
            playerAxisV = "P4_Vertical";
            playerButtonJump = "P4_A";
            playerButtonPickUp = "P4_X";
            playerButtonDash = "P4_LB";
            playerButtonAttack = "P4_RB";
        }
    }

   
}