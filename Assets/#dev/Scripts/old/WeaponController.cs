﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Создаёт массив из детей для данного GO
/// В один момент только 1 оружие активно
/// массив сканируеться только на Старте, т.е. надо вручную обновлять? массив если добавл новые пушки в процессе геймплея
/// </summary>


public class WeaponController : MonoBehaviour {
 
    private ArrayList weapons = new ArrayList();
    public int currentWeapon = 0;

    //
 	void Start () 
    {
        UpdateWeapons();
        ChangeWeapon(currentWeapon);
    }
	
	//
    void Update () 
    {

       

	}

    //Ищем всех детей и добвляем их в массив
    void UpdateWeapons()
    {
        foreach (Transform child in transform)
        {
            weapons.Add(child);
            //выключаем все объкты
            child.gameObject.SetActive(false);
        }
        //Debug.Log(weapons.Count);
        
    }


    //Меняет оружике на номер оруижя в массиве
    void ChangeWeapon(int weapSelect)
    {
        if (weapSelect < weapons.Count)
        {
            // Сделать текущее оружие не активным
            // можно ли попроще с листами обращаться ?
            Transform weapTmp = (Transform)weapons[currentWeapon];
            weapTmp.gameObject.SetActive(false);
            weapons[currentWeapon] = weapTmp;

            // Сделать новое оружие активным
            weapTmp = (Transform)weapons[weapSelect];
            weapTmp.gameObject.SetActive(true);
            weapons[weapSelect] = weapTmp;

            currentWeapon = weapSelect;
        }
       
    }

    //Меняет оружие след.
    void NextWeapon()
    {
        int weapNext;

        if ((currentWeapon + 1) < weapons.Count)
            weapNext = currentWeapon + 1;
        else
            weapNext = 0;

        // Сделать текущее оружие не активным
        Transform weapTmp = (Transform)weapons[currentWeapon];
        weapTmp.gameObject.SetActive(false);
        weapons[currentWeapon] = weapTmp;

        // Сделать след. оружие оружие активным
        weapTmp = (Transform)weapons[weapNext];
        weapTmp.gameObject.SetActive(true);
        weapons[weapNext] = weapTmp;

       
        currentWeapon = weapNext;

       
    }

    //Меняет оружие пред.
    void PrevWeapon()
    {
        int weapNext;

        if ((currentWeapon - 1) >= 0)
            weapNext = currentWeapon - 1;
        else
            weapNext = weapons.Count -1;

        // Сделать текущее оружие не активным
        Transform weapTmp = (Transform)weapons[currentWeapon];
        weapTmp.gameObject.SetActive(false);
        weapons[currentWeapon] = weapTmp;

        // Сделать след. оружие оружие активным
        weapTmp = (Transform)weapons[weapNext];
        weapTmp.gameObject.SetActive(true);
        weapons[weapNext] = weapTmp;


        currentWeapon = weapNext;


    }

}