﻿using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
using Random = UnityEngine.Random;

// Перенести всё основное в pd2_wepon ?
// Стрельба зависит от меканим стейтов и анимаций! 
// выстрела не будет если меканим находиться не в рекйол стейте
// стреляе т если так стейта recoil
public class p2d_wPistol : p2d_Weapon
{
    public float reloadTime;
    public int clipSize = 10;
    public float fireRate = 1.0f;
    public int bulletCount = 1;
    public GameObject bulletPrefab;
    public Transform instantPos;

    //разброс
    public float bulletSpreading;
    public float bulletSpreadingPosition;

    private float lastShoot;
    private int def_clipSize;

    public float knockBack = 30.0f;
    public float shakeStrenght = 0.01f;

    private void Start()
    {
        def_clipSize = clipSize;
    }

    //
    private void Update()
    {
    }

    private void TryFire()
    {

        if (clipSize > 0)
        {
            //Do something if this particular state is palying


            if (Time.time > lastShoot)
            {
                int x = 0;
                while (x < bulletCount)
                {

                    var bullet = Instantiate(bulletPrefab, instantPos.position, instantPos.rotation) as GameObject;
                    bullet.SendMessage("FireVector", GetComponentInParent<Crosshair>().crosshairObj.transform.position);
                       


                    x++;
                }

                //Двигаем игрока
                // ДуМув не дает двигать персонажа, во время стреьбы
                GetComponentInParent<Rigidbody2D>()
                    .DOMoveX(-1*knockBack*GetComponentInParent<p2d_PlayerMovement>().PlayerDirection, 0.1f)
                    .SetRelative();
                Camera.main.DOShakePosition(1f, shakeStrenght, 10, 30f);


                lastShoot = Time.time + fireRate;
                clipSize--;
            }
            //проверить как работает Time.time и замедление времени
        }
        else
        {
            //reload
            lastShoot = Time.time + reloadTime;
            clipSize = def_clipSize;
        }

       
    }


}