﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class p2d_HelathController : MonoBehaviour
{
    public float currentHP = 100.0f;
    public Slider sliderHP;

    private float defaultHp;
    private bool skipSlider = false;

    public bool destroyObject = false;
    public bool spawnDebris = false;
    public GameObject debris;

    void Start()
    {
        if (!spawnDebris)
            debris = null;

        defaultHp = currentHP;

        if (sliderHP == null)
            skipSlider = true;
    }


    void Update()
    {
        //проверка HP
        if (currentHP <= 0.0f)
            OnDie();


        // Запись в UI
        if (!skipSlider)
        {
            sliderHP.value = currentHP;
        }
    }


    /// <summary>
    /// Получение урона
    /// </summary>
    /// <param name="myDamage"></param>
    void OnDamage(float myDamage)
    {
        currentHP -= myDamage;
    }

    /// <summary>
    /// Подумать зачем две одинаковые фкнции урона
    /// </summary>
    /// <param name="someDamage"></param>
    void TryDamage(float someDamage)
    {
        OnDamage(someDamage);
    }


    /// <summary>
    /// Смерть игрока
    /// </summary>
    void OnDie()
    {
        //респаун
        SendMessage("OnRespawn",SendMessageOptions.DontRequireReceiver);

        if (spawnDebris)
            Instantiate(debris, transform.position, transform.rotation);


        if (destroyObject)
            Destroy(gameObject);


    }

    void Refresh()
    {
        currentHP = defaultHp;
      
    }

}
