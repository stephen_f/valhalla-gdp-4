﻿using UnityEngine;
using System.Collections;

public class Energy : MonoBehaviour
{


    public float HealthEnergy = 10;



	void Start () 
    {
	
	}


    //дэмдж low
    void lowDamage(float someDamage)
    {
//        GetComponent<Feedback>().OnDamage(Color.red, Color.white, 0.1f);



        HealthEnergy -= someDamage;


        if (HealthEnergy <= 0)
        {
            HealthEnergy = 0;
            OnDie();
        }
    }

    private void OnDie()
    {
        if (GetComponent<SmartCube>().Mounted)
        {
            SendMessageUpwards("CubeDestroy",GetComponent<SmartCube>().index);
        }
        else
        {
            GetComponent<SmartCube>().SendMessage("SelfDestruct");
        }
        
    }
}
