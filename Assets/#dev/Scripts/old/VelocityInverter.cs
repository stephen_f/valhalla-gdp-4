﻿using UnityEngine;
using System.Collections;

public class VelocityInverter : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

//    void OnTriggerEnter2D(Collider2D coll)
//    {
//        //Debug.Log(coll);
//        if (coll.gameObject.tag == "Bullet")
//        {
//            coll.GetComponent<Rigidbody2D>().velocity = new Vector2(coll.GetComponent<Rigidbody2D>().velocity.x * -1, coll.GetComponent<Rigidbody2D>().velocity.y * -1);
//        }
//    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            collision.collider.GetComponent<p2d_Bullet>().offColEnter = true;
            collision.collider.GetComponent<Rigidbody2D>().velocity = new Vector2(collision.collider.GetComponent<Rigidbody2D>().velocity.x * -1, collision.collider.GetComponent<Rigidbody2D>().velocity.y * -1);
        }
    }


}
