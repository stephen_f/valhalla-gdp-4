﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    private GameObject[] players;
    public Rect Rectangle;
    public float cameraSmoothFactor = 1;

    private double frustumWidth;
    private double distance;

    private double zMod = 1;
    public float DistanceMod = 1;
    private double wMod = 1;


    void Start()
    {


    }



    private void SearchPlayers()
    {
        Rectangle.Set(0, 0, 0, 0);

        
        // Ищет все объкты с тэгом игрок и запоолняет массив
        players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.transform.position.x > Rectangle.xMax)
                Rectangle.xMax = player.transform.position.x;


            if (player.transform.position.x < Rectangle.xMin)
                Rectangle.xMin = player.transform.position.x;


            if (player.transform.position.y < Rectangle.yMax)
                Rectangle.yMax = player.transform.position.y;

            if (player.transform.position.y > Rectangle.yMin)
                Rectangle.yMin = player.transform.position.y;
        }


    }

    void LateUpdate()
    {
        
        SearchPlayers();

        frustumWidth = (Rectangle.size.magnitude) * wMod;

        var frustumHeight = frustumWidth  / GetComponent<Camera>().aspect ;
        distance = frustumHeight * 0.5 / Mathf.Tan(GetComponent<Camera>().fieldOfView * 0.5f * DistanceMod * Mathf.Deg2Rad);

        var nPos = new Vector3(Rectangle.center.x, Rectangle.center.y, (float) (distance*zMod));
        transform.position = Vector3.Lerp(transform.position, nPos, Time.deltaTime*cameraSmoothFactor);


    }

   
}
