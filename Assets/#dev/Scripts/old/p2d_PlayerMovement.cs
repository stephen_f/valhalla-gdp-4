﻿using UnityEngine;

public class p2d_PlayerMovement : MonoBehaviour
{
    public float WalkSpeed = 10.0f;

    public float PlayerDirection = 1;
    public bool AirControl = false;
    public float CheckRadius = 0.2f;
    public Transform GroundCheck;
    public bool Grounded;
    public float JumpControl = 6.5f;
    public float JumpHeight = 4.0f;
    public LayerMask LayerGround;
    public float MultipleJumpHeight = 2.0f;
    public int MultipleJumps = 1;


    public bool _isStrafe = false;
    private float _jumpControl;
    private float _walkSpeed;
    public bool CanMove = true;
    private bool _jumpAddDone = false;
    private float _jumpHeightCurrent;
    private int _jumpsLeft;
    private float _nexTimetJumpAdd = 0;
    private string _playerAxisH;
    private string _playerAxisV;
    private float _jumpButtonPressed;


    private PlayersButtons _buttons;





   
    private void Start()
    {
       
        _buttons = GetComponent<PlayerInput>().Buttons;
        _jumpsLeft = MultipleJumps;


        //начальные значения, надо бы их починить, а то в рантайме изменять не могу
        _jumpControl = JumpControl;
        _walkSpeed = WalkSpeed;
    }

    private void Update()
    {
        
        AddMass();
    }

 

    private void FixedUpdate()
    {
        if (CanMove)
        {
            Grounded = Physics2D.OverlapCircle(GroundCheck.position, CheckRadius, LayerGround);

            Movement();
            MovementInAir();
            JumpFromGround();
            FlipSprite();
        }
    }

    private void AddMass()
    {
        GetComponent<Rigidbody2D>().mass = GetComponentInChildren<CubeController>().MassSum;
    }

    
    private void MovementInAir()
    {
        if (!Grounded && AirControl)
        {
            var axisX = Input.GetAxis(_buttons.playerAxisH);
            GetComponent<Rigidbody2D>().velocity = new Vector2(axisX * JumpControl,
                GetComponent<Rigidbody2D>().velocity.y);
        }

        
    }

    private void Movement()
    {
        if (Grounded)
        {
            var axisX = Input.GetAxis(_buttons.playerAxisH);
            GetComponent<Rigidbody2D>().velocity = new Vector2(axisX * WalkSpeed,
                GetComponent<Rigidbody2D>().velocity.y);
        }
        
    }

    private void JumpFromGround()
    {
        if (Grounded)
        {
            if (Input.GetButtonDown(_buttons.playerButtonJump))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, JumpHeight);
            }
        }
    }

    private void FlipSprite()
    {
        if (!Input.GetButton(_buttons.playerButtonRun))
        {
            if (Input.GetAxis(_buttons.playerAxisH) < -0.5f)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);

                PlayerDirection = -1;
            }
            else if (Input.GetAxis(_buttons.playerAxisH) > 0.5f)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);

                PlayerDirection = 1;
            }
        }
    }
}