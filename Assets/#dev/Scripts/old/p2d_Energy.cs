﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class p2d_Energy : MonoBehaviour
{
    public float currentEnergy = 100.0f;
    public Slider sliderEnergy;

    private float defaultEnergy;

    // skipSlider?
    private bool skipSlider = false;
    private float lastTime;
    public bool canRegen = false;

    public Transform SpriteFeedback;

    public int hp = 1;
    //regen
    public float regenDelay = 1.0f;
    public float regenPower = 10.0f;
    public float regenFrequency = 1.0f;
    private float timeDelay;

    void Start()
    {

        defaultEnergy = currentEnergy;

        if (sliderEnergy == null)
            skipSlider = true;
    }


    void Update()
    {
        //
        if (currentEnergy < 0)
            currentEnergy = 0;


        //regendelay
        if (Time.time > timeDelay)
            canRegen = true;

        // Запись в UI
        if (!skipSlider)
        {
            sliderEnergy.value = currentEnergy;
        }

        //реген
        if (canRegen && (currentEnergy < defaultEnergy))
        {
            if (Time.time > lastTime)
            {

                currentEnergy += regenPower;
                
                lastTime = Time.time + regenFrequency;
            }
            
            
        }

        //фикс макс хп
        if (currentEnergy > defaultEnergy)
        {
            currentEnergy = defaultEnergy;
        }
    }

    //Переименовать с приставкой On

    //дэмдж low
    void lowDamage(float someDamage)
    {



        currentEnergy -= someDamage;
        pauseRegen();
    }

    //дэмдж med
    void medDamage(float someDamage)
    {

        currentEnergy -= someDamage;

        if (currentEnergy <= 0)
        {
            HpDamage();
        }
    }

    //дэмдж high
    void highDamage(float someDamage)
    {

       OnDie();
    }


    /// <summary>
    /// Смерть игрока
    /// </summary>
    void OnDie()
    {
        //очки
        PlayerInput playerInput = GetComponent<PlayerInput>();
        //Ищет на сцене объект Scripts и отправляет всем его дочерним объектам сообщение
        GameObject scriptsGO = GameObject.Find("Scripts");
        scriptsGO.BroadcastMessage("subsPoints", playerInput.PlayerNumber.ToString());
        
        //респаун
        SendMessage("OnRespawn", SendMessageOptions.DontRequireReceiver);

    }

    void HpDamage()
    {
        hp--;

        if (hp <= 0)
        {
            OnDie();
        }
    }

    void Refresh()
    {
        currentEnergy = defaultEnergy;

    }

    void pauseRegen()
    {
        canRegen = false;
        timeDelay = Time.time + regenDelay;
    }

}
