﻿using UnityEngine;
using System.Collections;

public class p2d_Bullet : MonoBehaviour
{
    public float speed = 1.0f;
    public float damage = 1.0f;
    public Vector2 targetV;
    public GameObject collPrefab;
    public float acceleration = 0.0f;
    public bool offColEnter = false;
    public float lifeTime = 1.0f;


    //Define Enum
    public enum damageTypeEnum
    {
        lowDamage,
        medDamage,
        highDamage
    };

    //This is what you need to show in the inspector.
    public damageTypeEnum damageType;

    private void Start()
    {

        Invoke("OnDeath", lifeTime);
    }


    private void FireVector(Vector3 target)
    {

        GetComponent<Rigidbody2D>().velocity = (target - transform.position).normalized * speed;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        coll.collider.gameObject.SendMessage(damageType.ToString(), damage, SendMessageOptions.DontRequireReceiver);

        
        if (coll.gameObject.tag == "Bullet")
        {
            // подумать что
        }
        else
        {
            if (!offColEnter)
            {
                ContactPoint2D contactP = coll.contacts[0];
                Instantiate(collPrefab, contactP.point, Quaternion.identity);
                OnDeath();
            }
        }
    }

    private void OnDeath()
    {
        Destroy(gameObject);
    }
}