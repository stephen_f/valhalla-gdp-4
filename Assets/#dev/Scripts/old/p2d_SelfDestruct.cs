﻿using UnityEngine;
using System.Collections;

public class p2d_SelfDestruct : MonoBehaviour 
{
    public float timer = 5.0f;
	
	void Start () 
	{


        Invoke("SelfDestruct", timer);
	}
	
	void SelfDestruct ()
    {
        Destroy(gameObject);
    }

	void Update () 
	{
	
	}
}
