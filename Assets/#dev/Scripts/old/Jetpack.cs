﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Jetpack : MonoBehaviour
{
    public ParticleSystem JetPackPartlParticleSystem;
    public GameObject JetPackFading;
    private bool jetFlyMode;

    public void Start()
    {
        //JetPackFading.transform.DOScale((new Vector3(0.5f,0.5f,0.5f)), 0.5f).SetLoops(-1).SetEase(Ease.OutQuint);
        JetPackFading.GetComponent<Renderer>().material.DOFade(0, 1.5f);
        

    }


    private void jetPackControl(bool control)
    {
        if (control != jetFlyMode)
        {
            jetFlyMode = control;

            if (control)
            {
                jetPackSequence(true);
            }
            else
            {
                jetPackSequence(false);
            }
        }
    }

    private void jetPackSequence(bool start)
    {
        if (start)
        {
            JetPackPartlParticleSystem.enableEmission = true;
            //Invoke("EnableParticle", 0.01f);
            GetComponent<AudioSource>().Play();
            JetPackFading.GetComponent<Renderer>().material.DOFade(1, 1.5f);
        }
        else
        {
            GetComponent<AudioSource>().Stop();
            JetPackPartlParticleSystem.enableEmission = false;
            JetPackFading.GetComponent<Renderer>().material.DOFade(0, 1.5f);
        }
    }

    // у партиклов есть баг, если стопнуть их из скрипта, перенести в другое место и запустить будет виден шлейф
    // пришлось запсукать их через 0.01 сек
    public void EnableParticle()
    {
        JetPackPartlParticleSystem.enableEmission = true;

    }
}