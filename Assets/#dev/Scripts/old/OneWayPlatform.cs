﻿using UnityEngine;
using System.Collections;

//
//Триггер, при заходе игрока в который отключаютсья столкновения2d у layer1 и layer2 
//

public class OneWayPlatform : MonoBehaviour 
{


    [Header("Названия слоёв")]
    public string layer1;
    public string layer2;

    private bool oneway;

    void Start () 
	{
        oneway = false;
	}

    void Update () 
	{

	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(layer1), LayerMask.NameToLayer(layer2), true);


        

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer(layer1), LayerMask.NameToLayer(layer2), false);


    }
}
