﻿using UnityEngine;

public class Crosshair : MonoBehaviour
{
    private PlayersButtons _buttons;
    public GameObject crosshairObj;
    private float radius = 2f;
    public float radiusMult = 2f;
    public GameObject SpritePrefab;
    private Vector3 ShootVector;
    private void Start()
    {
        _buttons = GetComponent<PlayerInput>().Buttons;

        crosshairObj = Instantiate(SpritePrefab, transform.position, transform.rotation) as GameObject;
        crosshairObj.transform.SetParent(gameObject.transform);
    }

    private void Update()
    {
        if (GetShotingVector().normalized.magnitude > 0)
        {
            ShootVector = GetShotingVector();
        }


     

        crosshairObj.transform.position = ShootVector;

        radius = GetComponentInChildren<CubeController>().CubeStackList.Count*radiusMult;
    }

    private Vector3 GetShotingVector()
    {
        var shootV = new Vector2(Input.GetAxis(_buttons.playerAxisH2), Input.GetAxis(_buttons.playerAxisV2));
        var cubeStackLastCube = GetComponentInChildren<CubeController>().CubeStackList[GetComponentInChildren<CubeController>().CubeStackList.Count - 1];
        return
            cubeStackLastCube.transform.position + (Vector3) shootV.normalized*radius;
    }
}