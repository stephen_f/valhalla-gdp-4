﻿using UnityEngine;
using System.Collections;

public class p2d_SkillsController : MonoBehaviour
{
    //Ждёт нажатия клавиш и отправляет сообщение скиллам
    public string skillName1;
    public string skillName2;
    public string skillName3;
    private PlayersButtons _buttons;


    // Use this for initialization
    private void Start()
    {
        _buttons = GetComponent<PlayerInput>().Buttons;
    }


    private void Update()
    {
        //Скиллы
        if (Input.GetButton(_buttons.playerButtonRun))
            useSkill(skillName1);
        if (Input.GetButton(_buttons.playerButton2))
            useSkill(skillName2);
        if (Input.GetButton(_buttons.playerButtonPickUp))
            useSkill(skillName3);
    }

    private void useSkill(string skillName)
    {
        BroadcastMessage(skillName, SendMessageOptions.DontRequireReceiver);
    }
}