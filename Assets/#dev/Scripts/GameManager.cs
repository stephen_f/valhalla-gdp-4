﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;


/// <summary>
/// The game manager is a persistent singleton that handles points and time
/// </summary>
public class GameManager : PersistentSingleton<GameManager>
{
    /// the current number of game points
    public int Points { get; private set; }
    /// the current time scale
    public float TimeScale { get; private set; }
    /// true if the game is currently paused
    public bool Paused { get; set; }

    

    [Header("Settings")]
    [Space(10)]
    ///NumberOfPlayers
    public int PlayersCountSetting;

    public List<Player.PlayerNumber> PlayersToSpawn = new List<Player.PlayerNumber>();

    public GameObject[] FacesSetting;


    // storage
    private float _savedTimeScale;

    private List<Player> Players = new List<Player>();

    [SerializeField]
    public GameObject PlayerPrefab;

    
    public void SpawnAllPlayers()
    {
       
        if (PlayersToSpawn.Count <= 0)
        {
            //Савним по очереди
            for (int i = 0; i < PlayersCountSetting; i++)
            {
                SpawnPlayer((Player.PlayerNumber)i);
            }
        }
        else
        {
            //спавним из пула

            foreach (var playerToSpawn in PlayersToSpawn)
            {
                SpawnPlayer(playerToSpawn);
            }

        }
    }

    /// <summary>
    /// Создаёт всее объекты Player и респавнит их
    /// </summary>
    public void CreatePlayers()
    {
        if (Players.Count >= 4)
            return;

        for (int i = 0; i < 4; i++)
        {
            Players.Add(new Player());
            Players[i].PlayerState = Player.State.Inactive;
        }
        
    }

    /// <summary>
    /// Спавн игроков
    /// </summary>
    /// <param name="PlayerNumber"></param>
    public void SpawnPlayer(Player.PlayerNumber PlayerNumber)
    {
        var _Player = Players[(int) PlayerNumber];

        if (!_Player.CanSpawn)
            return;

        _Player.CanSpawn = false;
        _Player.PlayerState = Player.State.Active;

        //Face
        PlayerPrefab.GetComponent<CubeController>().FacePrefab = FacesSetting[(int)PlayerNumber];

        var playerGameObject = Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        _Player.PlayerGameObject = playerGameObject;

        //Настройки управления
        _Player.PlayerGameObject.GetComponent<PlayerInput>().PlayerNumber = PlayerNumber;

       
    }

    public void NewPosition(Player.PlayerNumber playerNumber,Vector3 setPoition)
    {
       if(Players[(int)playerNumber].PlayerGameObject == null)
            return;

        //Это метод очень долгий, подумать над более быстрым (он не срабатывает после Start())
       
        Players[(int) playerNumber].PlayerGameObject.transform.position = setPoition;
    }

    public void PlayerSetMove(Player.PlayerNumber PlayerNumb, bool CanMove)
    {

        var _player = Players[(int)PlayerNumb];

        _player.CanMove = CanMove;
        _player.PlayerGameObject.GetComponent<PlayerInput>().enabled = CanMove;

    }

    /// <summary>
    /// Убить игрока
    /// </summary>
    /// <param name="PlayerNumb"></param>
    public void KillPlayer(Player.PlayerNumber PlayerNumb)
    {
        var _Player = Players[(int)PlayerNumb];


        Destroy(_Player.PlayerGameObject);

        _Player.CanSpawn = true;
        _Player.PlayerState = Player.State.Dead;

        Debug.Log(PlayerNumb +" Умер");

        // Проверить сколько осталось в живых
        if (CountAlive() <= 1)
        {
            LevelManager.Instance.NextRound();
        }

    }
    /// <summary>
    /// Вовзращает кол-во живых игроков
    /// </summary>
    /// <returns></returns>
    public int CountAlive()
    {
        int AlivePlayers = 0;

        foreach (var _player in Players)
        {
            if (_player.PlayerState == Player.State.Active)
            {
                AlivePlayers ++;
            }
        }
        return AlivePlayers;
    }
    /// <summary>
    /// Возвращает Player от посленего живого игрока
    /// </summary>
    /// <returns></returns>
    public Player LastAlive()
    {
        if (CountAlive() <= 1)
        {
            foreach (var _player in Players)
            {
                if (_player.PlayerState == Player.State.Active)
                {
                    return _player;
                }
            }
        }
        
        return null;
        
    }

    public void SetMoveAllPlayers(bool CanMove)
    {
        foreach (var _player in Players)
        {
            SetMove(_player.Number, CanMove);
        }
    }

    public void SetMove(Player.PlayerNumber PlayerNumb, bool CanMove)
    {
        if (Players[(int)PlayerNumb].PlayerGameObject == null)
            return;

        Players[(int)PlayerNumb].PlayerGameObject.GetComponent<PlayerInput>().enabled = CanMove;
        Players[(int)PlayerNumb].CanMove = CanMove;


    }

    public void AddPlayerScore(Player.PlayerNumber Score, int PlayerNumb)
    {
        throw new System.NotImplementedException();
    }

    public void RemovePlayers()
    {
         Players.Clear();
    }


    // ----------------old

    /// <summary>
    /// this method resets the whole game manager
    /// </summary>
    public void Reset()
    {
        Points = 0;
        TimeScale = 1f;
        Paused = false;
        GUIManager.Instance.RefreshPoints();
    }

    /// <summary>
    /// Adds the points in parameters to the current game points.
    /// </summary>
    /// <param name="pointsToAdd">Points to add.</param>
    public void AddPoints(int pointsToAdd)
    {
        Points += pointsToAdd;
        GUIManager.Instance.RefreshPoints();
    }

    /// <summary>
    /// use this to set the current points to the one you pass as a parameter
    /// </summary>
    /// <param name="points">Points.</param>
    public void SetPoints(int points)
    {
        Points = points;
        GUIManager.Instance.RefreshPoints();
    }

    /// <summary>
    /// sets the timescale to the one in parameters
    /// </summary>
    /// <param name="newTimeScale">New time scale.</param>
    public void SetTimeScale(float newTimeScale)
    {
        _savedTimeScale = Time.timeScale;
        Time.timeScale = newTimeScale;
    }

    /// <summary>
    /// Resets the time scale to the last saved time scale.
    /// </summary>
    public void ResetTimeScale()
    {
        Time.timeScale = _savedTimeScale;
    }

    /// <summary>
    /// Pauses the game
    /// </summary>
    public void Pause()
    {
        // if time is not already stopped		
        if (Time.timeScale > 0.0f)
        {
            Instance.SetTimeScale(0.0f);
            Instance.Paused = true;
            GUIManager.Instance.SetPause(true);
        }
        else
        {
            Instance.ResetTimeScale();
            Instance.Paused = false;
            GUIManager.Instance.SetPause(false);
        }
    }
    /// <summary>
    /// Тут будут режимы игры
    /// </summary>
    /// <param name="level"></param>
    public void OnLevelWasLoaded(int level)
    {
        //В анчале раунда сбрасывает CanSPawn
        foreach (var _player in Players)
        {
            _player.CanSpawn = true;

        }
    }

}
