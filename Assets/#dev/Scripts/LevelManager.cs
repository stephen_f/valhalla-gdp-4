﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

/// <summary>
/// Spawns the player, and 
/// </summary>
public class LevelManager : PersistentSingleton<LevelManager>
{
	/// Singleton
	public static LevelManager Instance { get; private set; }		
	/// the elapsed time since the start of the level
	public TimeSpan RunningTime { get { return DateTime.UtcNow - _started ;}}
	
	// private stuff
	private DateTime _started;
    public float StartCounterDuration = 1f;
    public float EndDuration = 2f;


    /// <summary>
	/// On awake, instantiates the player
	/// </summary>

	
	/// <summary>
	/// Initialization
	/// </summary>
	public void Start()
	{
        Instance = this;
        _started = DateTime.UtcNow;
	    StartCoroutine(StartLevel());
	}


    public void OnLevelWasLoaded(int level)
    {
        if (level == 0)
        {
            StartCoroutine(Menulevel());
        }
        else
        {
            StartCoroutine(StartLevel());
        }
        
    }

    private IEnumerator Menulevel()
    {
        yield return new WaitForSeconds(0.01f);
        GameManager.Instance.CreatePlayers();
        
        // GameManager.Instance.SpawnAllPlayers();
        // MenuPositions();
    }

    private IEnumerator StartLevel()
    {
        yield return new WaitForSeconds(0.01f);
        GameManager.Instance.CreatePlayers();
        GameManager.Instance.SpawnAllPlayers();
        RespawnPositions();
//        GameManager.Instance.SetMoveAllPlayers(false);
//        yield return new WaitForSeconds(StartCounterDuration);
//        GameManager.Instance.SetMoveAllPlayers(true);
        
    }

    public void NextRound()
    {
        StartCoroutine(LevelEnd());

    }
    public void SkipRound()
    {
        LoadRandomLevel();

    }
    public void RespawnPositions()
    {
        Respawn spawns = FindObjectOfType<Respawn>();
        

        for (int i = 0; i < GameManager.Instance.PlayersCountSetting; i++)
        {
            GameManager.Instance.NewPosition((Player.PlayerNumber) i, spawns.GetPostionAt(0));
        }
    }

    public void MenuPositions()
    {
        Respawn spawns = FindObjectOfType<Respawn>();

        for (int i = 0; i < GameManager.Instance.PlayersCountSetting; i++)
        {
            GameManager.Instance.NewPosition((Player.PlayerNumber)i, spawns.GetOrderPosition());
        }
    }
    public void MenuPositions(Player.PlayerNumber PlayerNumber)
    {
        Respawn spawns = FindObjectOfType<Respawn>();
        GameManager.Instance.NewPosition(PlayerNumber, spawns.GetOrderPosition());
        
    }
    //Двигает камеру  на оставшевогся игрока и через 2 секунды завершает ранунд
    private IEnumerator LevelEnd()
    {
        MoveCam();

        yield return new WaitForSeconds(EndDuration);

        LoadRandomLevel();

    }

    private void MoveCam()
    {
        
        var lastAlive = GameManager.Instance.LastAlive().PlayerGameObject;
        var lastAlivePos = lastAlive.transform.position +
                           new Vector3(0, 0, 10f);

        Camera.main.GetComponent<CameraController>().enabled = false;
        Camera.main.transform.DOMove(lastAlivePos, EndDuration);

        Camera.main.DOFieldOfView(60f, EndDuration);
    }

    private static void LoadRandomLevel()
    {
        Application.LoadLevel(UnityEngine.Random.Range(1, Application.levelCount));
    }
}

