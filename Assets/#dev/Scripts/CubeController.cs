﻿using System;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;


public class CubeController : MonoBehaviour
{

    public List<Transform> CubePositionObjects;
    public List<SmartCube> CubeStackList;

    public SmartCube StartCubeBody;
    public SmartCube StartCubeLegs;
    public GameObject FacePrefab;

    public bool Full = false;

    public GameObject Face;
    public float MassSum;
    //начальные кубы, при которых можно убить игрока
    public int CoreCubes = 2;

    public GameObject CubePositionsPrefab;
    private GameObject _cubePositionsPrefab;

    public float WalkStatStartValue = 10f;
    private CharacterBehavior _character;
    public float minSpeed = 2f;
    public float maxSpeed = 10f;


    [ShowOnly]
    public int cubesCount;

    public int CubesCount
    {
        get
        {
            cubesCount = CubeStackList.Count;
            return cubesCount;
        }
    }


    // Use this for initialization
    private void Start()
    {
        _cubePositionsPrefab = Instantiate(CubePositionsPrefab, transform.position, transform.rotation) as GameObject;
        _cubePositionsPrefab.transform.SetParent(gameObject.transform);
        _character = GetComponent<CharacterBehavior>();

        SearchPositionsInChilds();
        CubeAdd(StartCubeBody);
        CubeAdd(StartCubeLegs);
    }

    private void SearchPositionsInChilds()
    {
        foreach (var child in _cubePositionsPrefab.GetComponentsInChildren<Transform>())
        {
            if (child.parent == _cubePositionsPrefab.transform)
            {
                CubePositionObjects.Add(child);
                //Debug.Log(child);
            }
        }
    }



    private void CubeDestroy(int index)
    {

        Full = false;

        AddMass(-CubeStackList[index].GetComponent<SmartCube>().Mass);


        Destroy(CubeStackList[index].gameObject, 0.1f);
        CubeStackList.RemoveAt(index);

        CubeUpdate();
    }

    public void CubeDestroySafe(int index)
    {
        if (CubesCount <= CoreCubes)
            return;

        if (index == 0)
        {
            CubeDestroy(index +  1);
            return;
        }
        if (index == (cubesCount - 1))
        {
            CubeDestroy(index - 1);
            return;
        }

        CubeDestroy(index);

    }

    public void DashCost()
    {
        CubeDestroySafe(0);
        
    }

    public void CubeAdd(SmartCube CubeGO)
    {
        if (CubesCount >= CubePositionObjects.Count)
        {
            Full = true;
            return;
        }

        var clone = (SmartCube)Instantiate(CubeGO, transform.position, transform.rotation);

        if (CubesCount >= CoreCubes)
        {
            //вставляет после ног
            CubeStackList.Insert(1, clone);
        }
        else
        {
            CubeStackList.Insert(0, clone);
        }




        CubeUpdate();
        AddMass(clone.GetComponent<SmartCube>().Mass);


    }

    private void AddMass(float mass)
    {
        MassSum += mass;
    }

    //Обновляет позиции всех кубов
    private void CubeUpdate()
    {
        Transform curPostionTemplate = CubePositionObjects[CubesCount - 1];
        Transform[] curPostions = curPostionTemplate.GetComponentsInChildren<Transform>();

        int i = 0;
        foreach (var pos in curPostions)
        {
            if (i != 0)
            {
                CubeStackList[i - 1].index = i - 1;
                CubeStackList[i - 1].transform.SetParent(pos);
                CubeStackList[i - 1].transform.DOLocalMove(pos.localPosition, 0.1f).SetEase(Ease.InOutBounce);

                CubeStackList[i - 1].SetModeHead(false);
                CubeStackList[i - 1].SetModeLeg(false);
            }

            i++;
        }

        
        

        SetFace();
        PlayerStats();
        CheckCore();
    }

    private void CheckCore()
    {
        CubeStackList[0].SetModeLeg(true);
        CubeStackList[CubesCount - 1].SetModeHead(true);

        if (CubesCount <= CoreCubes)
        {
            SetCanDead(0, true);
            SetCanDead(CubesCount - 1, true);
        }
        else
        {
            SetCanDead(0, false);
            SetCanDead(CubesCount - 1, false);
        }
    }

    private void PlayerStats()
    {
        if (CubesCount == CoreCubes)
        {
            _character.Permissions.JetpackEnabled = true;
            _character.Permissions.ShootEnabled = false;

        }
        else
        {
            _character.Permissions.JetpackEnabled = false;
            _character.Permissions.ShootEnabled = true;
        }

        var speedMod = (2f / CubesCount) * WalkStatStartValue;
        var StatSpeed = Mathf.Clamp(speedMod, minSpeed, maxSpeed);

        _character.BehaviorParameters.WalkSpeed = StatSpeed;
        _character.BehaviorParameters.MovementSpeed = StatSpeed;

    }


    private void SetCanDead(int cubeIndex, bool can)
    {
        if (CubeStackList[cubeIndex].GetComponent<HealthCore>() != null)
        {
            CubeStackList[cubeIndex].GetComponent<HealthCore>().CanDead = can;
        }
    }

    private void SetFace()
    {
        if (Face == null)
        {
            Face = Instantiate(FacePrefab, transform.position, transform.rotation) as GameObject;
        }

        Face.transform.position = CubeStackList[CubesCount - 1].GetComponent<SmartCube>().SlotHead.position;
        Face.transform.SetParent(CubeStackList[CubesCount - 1].GetComponent<SmartCube>().SlotHead);

    }

   
}