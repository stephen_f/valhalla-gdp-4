﻿using UnityEngine;
using System.Collections;

public class Shadow_Script : MonoBehaviour
{

    public bool castShadows;
    public bool receiveShadows;

    // Use this for initialization
    void Start()
    {
        GetComponent<Renderer>().castShadows = castShadows;
        GetComponent<Renderer>().receiveShadows = receiveShadows;
    }

    // Update is called once per frame
    void Update()
    {

    }
}