﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityStandardAssets.ImageEffects;

public class CaptureGIF : MonoBehaviour
{
    public int ScreenshotWidth = 512;
    public string ScreenshotDir = "PNGsForGIF";
    public string ScreenshotPrefix = "Capture_";

    private bool StartCapture = false;
    private int count = 0;
    /// <summary>
    /// Takes an asset path (starting with 'Asset')
    /// and returns the full (absolute) system path.
    /// </summary>
    static string GetFullPath(string aAssetPath)
    {
        // Get the full path of the Unity's project folder (without 'Assets')
        var projectPath = System.IO.Path.GetDirectoryName(Application.dataPath);

        // Combine it with the given asset path
        return System.IO.Path.Combine(projectPath, aAssetPath);
    }




    /// <summary>
    /// Run when you play the scene
    /// </summary>
    public void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.O))
        {
            //StartCapture = true;
            StartCoroutine(TakeSingleScreenshot());
        }
        if (StartCapture)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                Destroy(gameObject);
            }

            StartCoroutine(TakeSingleScreenshot());
        }

       

    }

    /// <summary>
    /// This is where we do all the work!
    /// </summary>
    IEnumerator TakeSingleScreenshot()
    {
        // Create the screenshot directory if necessary
        string basePath = GetFullPath(ScreenshotDir);
        if (!System.IO.Directory.Exists(basePath))
        {
            System.IO.Directory.CreateDirectory(basePath);
        }

        // Compute width and height based on the camera
        int width = (int)ScreenshotWidth;
        int height = (int)(ScreenshotWidth / Camera.main.aspect);

        // Create the render texture that we will use to capture the scene
        RenderTexture renderTexture = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

        // Tell Unity that it needs to use it
        var prevCameraTexture = Camera.main.targetTexture;
        Camera.main.targetTexture = renderTexture;

        // Wait until rendering has happened!
        yield return new WaitForEndOfFrame();

        // Copy the content of the render texture into a Texture2D (so we can access the texture buffer)
        var prevRenderTexture = RenderTexture.active;
        RenderTexture.active = renderTexture;
        Texture2D screenshotTexture = new Texture2D(width, height, TextureFormat.ARGB32, false);
        screenshotTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        screenshotTexture.Apply();

        // Encode the buffer to PNG and write it to disk!
        byte[] bytes = screenshotTexture.EncodeToPNG();
        string screenshotPathname = System.IO.Path.Combine(basePath, ScreenshotPrefix) + count + ".png";
        File.WriteAllBytes(screenshotPathname, bytes);

        // Clean up
        Destroy(screenshotTexture);
        Camera.main.targetTexture = prevCameraTexture;
        RenderTexture.active = prevRenderTexture;


        Debug.Log("Screenshot Captured!");
        count++;
        yield return new WaitForSeconds(0.04f);
        StartCoroutine(TakeSingleScreenshot());
    }
}
