﻿using UnityEngine;


public class PA_Char : ProcAnim
{
    public float VerticalMotion;

    void Update()
    {
        base.Update();


        ApplySpeed();
        ApplyVerticalMotion();
    }

    private void ApplySpeed()
    {
        _animator.SetFloat("Speed", GameManager.Instance.LastAlive().PlayerGameObject.GetComponent<CorgiController>().Speed.magnitude);
    }

    private void ApplyVerticalMotion()
    {
        VerticalMotion = Mathf.Clamp01(VerticalMotion);
        _animator.SetLayerWeight(_animator.GetLayerIndex("VerticalMotion"), VerticalMotion);
    }

    
}

