﻿using UnityEngine;
using DG.Tweening;


/// <summary>
/// autor: Stephen F.
/// url: https://trello.com/b/YLUDoUgk/puppet2d-wolfire-intrepolation-trick
/// Твинит значение в аниматоре в засимости от настроек.
/// ChangeState - обязательный триггер в аниматоре, для перехода между бледами
///  </summary>
public class ProcAnim : MonoBehaviour
{

    public float Value;
    public string AnimatorValue;

    public float MaxTween = 1f;
    public float Duration = 1f;
    public bool Clamp01;
    public Ease Interpolation;
    public LoopType Loop;
    

    public bool UseCurve;
    public AnimationCurve Curve;
    
    public bool DebugReset;

    //Cache
    protected Animator _animator;
    protected Sequence _sequence;


    public void Start()
    {
        _animator = GetComponent<Animator>();
       
        CreateSequence();
        CheckParameter();

    }

    private void CheckParameter()
    {
        if (!_animator.HasParameterOfType("ChangeState", AnimatorControllerParameterType.Trigger))
        {
            throw new System.Exception("Отсутствует обязательный триггер в аниматоре, <ChangeState>");
        }
    }


    public virtual void Update()
    {
        

        if (Clamp01)
        {
            Mathf.Clamp01(Value);
        }

        _animator.SetFloat(AnimatorValue, Value);
       

        if (DebugReset)
            ResetSequence();
    }
    
    //Создает твин
    private void CreateSequence()
    {
        // Create a new Sequence.
        _sequence = DOTween.Sequence();
        // Add a tween that will last the whole Sequence's duration
        _sequence.Append(DOTween.To(() => Value, x => Value = x, MaxTween, Duration));

        //Set interpolation
        if (!UseCurve)
            _sequence.SetEase(Interpolation);
        else
            _sequence.SetEase(Curve);

        // Set the whole Sequence to loop infinitely 
        _sequence.SetLoops(-1, Loop);

        //calllback every loop
        _sequence.OnStepComplete(OnEveryLoopComplete);
    }

    private void OnEveryLoopComplete()
    {
        _animator.SetTrigger("ChangeState");
    }

    //Пеерезсоздает твин, для отладки в редакторе
    private void ResetSequence()
    {
        DebugReset = false;
       
        _sequence.Rewind();
        _sequence.Kill();

        CreateSequence();
    }
}
