﻿using UnityEngine;
using System.Collections;

public class StrideWheel : MonoBehaviour
{
    //StrideWheel
    private Vector3 lastPosition;
    private float angleCounter;
    public float wheelStepDistance;
    public float wheelRadius;
    private Transform surveyorWheel;

    public float SpeedMod;


    // Use this for initialization
    void Start()
    {
        lastPosition = transform.position;
        
        //??
        InvokeRepeating("LastPosEvent", 0.1f, 0.1f);
    }
	
	// Update is called once per frame
	void Update ()
	{
	    DoStride();
	}

    private void DoStride()
    {
        Vector3 currPosition = new Vector3(transform.position.x, 0F, transform.position.z);

        float dist = Vector3.Distance(lastPosition, currPosition);
        float turnAngle = (dist/(2*Mathf.PI*wheelRadius))*360F;

        //For visualization. Attach to Transform.
        //surveyorWheel.Rotate(new Vector3(0F, 0f, -turnAngle));

        angleCounter += turnAngle;

        if (angleCounter > wheelStepDistance)
            angleCounter = 0F;

        SpeedMod = angleCounter / wheelStepDistance;
        

        if (SpeedMod > 1F)
            SpeedMod = 0f;


        
    }

    public void LastPosEvent()
    {

        lastPosition = new Vector3(transform.position.x, 0F, transform.position.z);
    }
}
